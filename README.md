# webservice

Learning webservice

Pada projek kali ini adalah learning dari tutorial membuat restful API / webservice menggunakan slim framework

Spesifikasi projek ini:
-PHP
-Mysql
-Slim framework
-Postman (untuk testing API)

Fitur dari projek ini:
-web service menggunakan slim framework
-Upload file ke webservice

Terima kasih kepada www.petanikode.com untuk tutorialnya, sangat terstruktur dan mudah untuk pemula
